module.exports = {
  plugins: [
    [
      "@init-rn-app/plugin-copy-template",
      { template: "react-native", projectName: "sampleApp" },
    ],
    "@init-rn-app/plugin-yarn-workspaces",
    [
      "@init-rn-app/plugin-set-bundle-id",
      {
        projectName: "sampleApp",
        bundleId: `com.test.sampleApp`,
      },
    ],
  ],
};
